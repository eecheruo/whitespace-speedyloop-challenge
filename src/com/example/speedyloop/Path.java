package com.example.speedyloop;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class Path {
    private static final String[] TERMINALS = { "A", "B", "C", "D", "E" };
    private final List<Route>[] routes;
    private List<String> paths;
    private int to;
    private int distance;
    private int stop;
    private int count;
    private int trip;

    public Path(String terminals)
    {
        this(TERMINALS.length);
        initialize(terminals);
    }

    public int getTrips(String from, String to, Predicate<Integer> predicate, int stop)
    {
        this.to = getIndex(to);
        this.stop = stop;
        this.trip = 0;
        int index = getIndex(from);
        calcTrips(index, String.valueOf(index), predicate);
        return this.trip;
    }

    public int getShortestDistance(String from, String to)
    {
        this.paths = new ArrayList<>();
        this.to = getIndex(to);
        int index = getIndex(from);
        calcShortestDistance(index, String.valueOf(index));
        int distance = Integer.MAX_VALUE, current;
        for (String path : this.paths)
        {
            current = calcDistance(path);
            if (distance > current) {
                distance = current;
            }
        }
        return (distance == Integer.MAX_VALUE) ? 0 : distance;
    }

    public String getDistance(String terminals) {
        int distance = calcDistance(terminals);
        return (distance != -1) ? String.valueOf(distance) : "NO SUCH ROUTE";
    }

    public int getRoutes(String from, String to, int distance)
    {
        this.to = getIndex(to);
        this.distance = distance;
        this.count = 0;
        int index = getIndex(from);
        calcRoutes(index, String.valueOf(index));
        return this.count;
    }

    private void calcRoutes(int from, String path)
    {
        List<Route> routes = getRoutes(from);
        for (Route route : routes)
        {
            String to = path + route.to();
            int distance = calcDistance(getPath(to));
            if (this.to == route.to() && distance < this.distance) {
               this.count++;
            }
            if (distance < this.distance) {
                calcRoutes(route.to(), to);
            }
        }
    }

    private Path(int count)
    {
        if (count < 0) throw new IllegalArgumentException("Number of routes must be positive integer");
        this.routes = (LinkedList<Route>[]) new LinkedList[count];
        for (int index = 0; index < count; index++) {
            this.routes[index] = new LinkedList<>();
        }
    }

    private static int getIndex(String terminal)
    {
        int index = Arrays.binarySearch(TERMINALS, terminal);
        if (index < 0) {
            throw new IndexOutOfBoundsException(MessageFormat.format("Terminal {0} cannot be found", terminal));
        }
        return index;
    }

    private void initialize(String terminals)
    {
        String[] terminalList = terminals.split(",");
        for(String terminal : terminalList) {
            int from = getIndex(terminal.trim().substring(0,1));
            int to = getIndex(terminal.trim().substring(1,2));
            int size = Integer.parseInt(terminal.trim().substring(2));
            Route route = new Route(from, to, size);
            this.addRoute(route);
        }
    }

    private void addRoute(Route route)
    {
        int index = route.from();
        this.routes[index].add(route);
    }

    private List<Route> getRoutes(int index)
    {
        if (index < 0 || index >= TERMINALS.length){
            throw new IndexOutOfBoundsException(MessageFormat.format("{0} is out of bounds", index));
        }
        return this.routes[index];
    }

    private int calcDistance(String terminals)
    {
        if (terminals == null) {
            throw new IllegalArgumentException("List of terminals are required");
        }

        int from, to, distance = 0;
        String[] terminalList = terminals.trim().split("");
        for (int i = 0; i < terminalList.length - 1;)
        {
            boolean found = false;
            from = getIndex(terminalList[i++]);
            to = getIndex(terminalList[i]);
            List<Route> routes = getRoutes(from);
            for (Route route : routes)
            {
                if (route.to() == to)
                {
                   distance += route.size();
                    found = true;
                    break;
                }
            }
            if (!found) {
               return -1;
            }
        }
        return distance;
    }

    private void calcTrips(int from, String path, Predicate<Integer> predicate)
    {
        List<Route> routes = getRoutes(from);
        for (Route route : routes)
        {
            String to = path + route.to();
            int stop = to.length() - 1;
            if (this.to == route.to() && predicate.test(stop)) {
                this.trip++;
            }
            if (stop <= this.stop) {
               calcTrips(route.to(), to, predicate);
            }
        }
    }

    private String getTerminal(int index)
    {
        if (index < 0 || index >= TERMINALS.length)
        {
           throw new IndexOutOfBoundsException("index out of bound");
        }
        return TERMINALS[index];
    }

    private String getPath(String path)
    {
        String[] pathList = path.trim().split("");
        StringBuilder name = new StringBuilder();
        for(String index : pathList)
        {
            name.append(getTerminal(Integer.parseInt(index)));
        }
        return name.toString();
    }

    private void calcShortestDistance(int from, String path)
    {
        List<Route> routes = getRoutes(from);
        for (Route route : routes)
        {
            if (path.length() > 1 && path.substring(1).contains(String.valueOf(route.to()))) {
               continue;
            }

            String to = path + route.to();
            if (this.to == route.to()) {
                this.paths.add(getPath(to));
            }
            calcShortestDistance(route.to(), to);
        }

    }

}
