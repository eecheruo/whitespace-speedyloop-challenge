package com.example.speedyloop;

public record Route(int from, int to, int size) {

    public Route {
        if (from < 0) throw new IllegalArgumentException("From must be positive integer");
        if (to < 0) throw new IllegalArgumentException("To must be positive integer");
        if (size < 0) throw new IllegalArgumentException("Size must be positive integer");
    }

}
