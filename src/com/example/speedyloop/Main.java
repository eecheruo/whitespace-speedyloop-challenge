package com.example.speedyloop;

import java.text.MessageFormat;

public class Main {
    public static void main(String[] args) {

        Path path = new Path("AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7");
        System.out.println(MessageFormat.format("Output #1: {0}", path.getDistance("ABC")));
        System.out.println(MessageFormat.format("Output #2: {0}", path.getDistance("AD")));
        System.out.println(MessageFormat.format("Output #3: {0}", path.getDistance("ADC")));
        System.out.println(MessageFormat.format("Output #4: {0}", path.getDistance("AEBCD")));
        System.out.println(MessageFormat.format("Output #5: {0}", path.getDistance("AED")));
        System.out.println(MessageFormat.format("Output #6: {0}", path.getTrips("C", "C", (trip -> trip <= 3), 3)));
        System.out.println(MessageFormat.format("Output #7: {0}", path.getTrips("A", "C", (trip -> trip == 4), 4)));
        System.out.println(MessageFormat.format("Output #8: {0}", path.getShortestDistance("A", "C")));
        System.out.println(MessageFormat.format("Output #9: {0}", path.getShortestDistance("B", "B")));
        System.out.println(MessageFormat.format("Output #10: {0}", path.getRoutes("C", "C", 30)));

    }
}
